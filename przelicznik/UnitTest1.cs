﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace przelicznik
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string sWaga  = "12";
            decimal sprawdzenie = 12;
            decimal waga;

            if (sWaga != "")
            {
                waga = Convert.ToDecimal(sWaga);
            }
            else
            {
                waga = 0;
            }

            Assert.AreEqual(waga, sprawdzenie);
        }
    }
}
